const express = require("express");
const config = require("./config/app.json");
const site = require("./routes/site");

const app = express();

const port = 20052;

const prefix = config.enviroment === "dev" ? "" : "/~nick/dagtentamen";

app.set("view engine", "ejs");
app.set("views", `${process.cwd()}/resources/views`);

app.use(`${prefix}/`, site);

app.listen(port, () => {
    console.log(`Website on ${port}`);
});
